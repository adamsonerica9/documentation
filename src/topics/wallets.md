---
title: Accounts and Wallets
hide:
  - toc
---

Accounts are payment network-specific classes that perform the following functions:

 - keep track of current balances from each of the tracked tokens
   available on the network
 - keep track of routing details that are needed for users that want
   to deposit or make payments *to* the hub
 - store and provide credentials to perform withdrawals *from* the hub

Depending on the use-case, you might be interested in using different
types of accounts on the same payment network. For example, if you
want to use Hub20 to accept payments via blockchain but you are not
interested in letting other users make payouts, it makes sense to use
"Cold" wallets, i.e, accounts that do not store any private keys and
therefore can not sign any transactions.

Currently, only the *ethereum* module defines Account models:

 - [ColdWallet](../reference/wallets.md#coldwallet) is the model that
   stores only the address, and can only receive payments
 - [KeystoreAccount](../reference/wallets.md#keystoreaccount) is a
   basic model that stores the private key in the database. This model
   allows hub operators to have an easy way to get started, but it is
   *not* recommended for production settings.
 - [HierarchicalDeterministicWallet](../reference/wallets.md#hierarchicaldeterministicwallet)
   is a model that uses can generate new addresses on demand from one
   single *seed phrase*. It is the current recommended method to be
   used for those that want to operate a hub that is able to execute
   transactions automatically.
