---
title: Cryptocurrencies and tokens
hide:
  - toc
---

!!! question "What is the difference between cryptocurrency and cryptotokens"

    In Hub20, you will be using the terms *tokens* to refer to
    any cryptocurrency  (as opposed to "fiat" currency), but for all intents
    and purposes the terms can be interexchangeable

### What is a cryptocurrency?

> A cryptocurrency is a medium of exchange secured by a
> blockchain-based ledger.

> A medium of exchange is anything widely accepted as payment for
> goods and services, and a ledger is a data store that keeps track of
> transactions. Blockchain technology allows users to make
> transactions on the ledger without reliance upon a trusted third
> party to maintain the ledger.

> -- <cite>[Ethereum Wiki - Intro to
> Ether](https://ethereum.org/en/developers/docs/intro-to-ether/)</cite>


### What tokens are supported by Hub20?

Currently, Hub20 implements only [payment
networks](./payment_networks/index.md) based on Ethereum blockchains,
and it supports only *fungible* tokens.

Ethereum-compatible blockchains defines two different types of tokens:

 - The blockchain's **native currency**, i.e, the currency used to pay
   for transaction fees. In the case of Ethereum, this native currency
   is called *Ether*.

 - **ERC20** (or sometimes called EIP20) tokens, which are defined as
   smart contracts that follow the
   [EIP-20](https://eips.ethereum.org/EIPS/eip-20) and provide all the
   common functionality expected from a *fungible* currency - i.e, it
   provides methods to determine the balance, the total token supply,
   to make transfers between different accounts, etc. Most of the
   Wallet software expects tokens to be following this standard.


### Selecting tokens to be supported by the hub

Considering that anyone can create new tokens on the blockchain (just
by creating a new smart contract and deploying on the blockchain), and
that there is **no guarantee** about the security and correctness of a
contract, hub operators need to have the utmost care when determining
what tokens are going to be accepted and traded by its users. Tokens
available to users are called *listed* in the API and the frontend
application.

As a hub operator, you can add and list tokens with two different methods:

 - By using the CLI and calling the
   [load_erc20_token](../reference/cli.md#load_erc20_token)
 - By using the admin website.

### Token Lists

Given that there are so many available tokens on the different
blockchains, the team behind [Uniswap](https://uniswap.org) has
created and is leading the development of
[TokenLists](https://tokenlists.org/), an initiative to have a
independently-published and community-curated documents providing
lists of token contracts that are deemed safe. The goal is to have a
system that can help users verify the information about any token by,
e.g, measuring the "reputation" of a token by checking in how many
lists a token is listed, or by pre-defining what tokens are allowed at
a dapp.

Each lists is a simple JSON document with information about the token
(name, logo, symbol) and the blockchain and address of the deployed
contract. Hub20 lets operators make use of these token lists by
proving a [command to load token list to the
database](../reference/cli.md#load_token_list).
