---
title: Hub20 In Detail
hide:
  - toc
---

This section will break down of the key concepts that are important to
understand when using or managing an instance of Hub20.

 - [Payment Networks](./payment_networks/index.md) is how Hub20
   abstracts any system used to transfer value. At the moment, Hub20
   provides integration with:is focused on systems built on or compatible with
    * [Ethereum](./payment_networks/ethereum_blockchains.md) and any
      blockchain that can be connected through nodes compatible with
      Ethereum's RPC API.
    * [Raiden](./payment_networks/raiden.md), a completely
      decentralized "layer-2" system that enables fast and cheap
      transfers.


- [Wallets](./wallets.md) in the context of crypto refer to the system
   that manages cryptographic keys that are used to interact with the
   blockchains and are the basis of all security aspects when dealing
   with smart contracts. Hub20 abstracts a lot of this complexity for
   the users, but hub operators need to have a deep understanding of
   how they work and undertand best practices to keep funds protected.

 - [Tokens](./tokens.md), more particularly ERC20-tokens, is a
   standard for smart contracts that can be used as cryptocurrencies
   on any Ethereum-compatible blockchains.

 - [Payments and Payment Routing](./payment_gateway.md) are important
   for those that want to accept cryptocurrencies online. We also
   discuss about the checkout system and how to integrate it with your
   existing site or e-commerce platform.

 - The [accounting](./accounting.md) module is important for hub
   operators, given that they will be managing funds from different
   people and need to be able to know how much each one can claim from
   the funds that are collectivelly held by the wallets.
