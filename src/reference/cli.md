Hub operators who have direct access to the server running their
instances can make use of the command-line utility (based on
[django-admin](https://docs.djangoproject.com/en/4.0/ref/django-admin/))

Below is a list of the commands available. You can also obtain more
information about these commands by calling `django-admin help` or
`django-admin <command_name> help`

## Commands that execute long-running processes

These commands execute long running processes and should be run as
*services* and not interactively.

### `run_payment_providers`

Starts a process and puts each active provider in a separate thread,
and calls the `run` method of each `PaymentProvider`.

Usage: `django-admin run_payment_providers```


### `run_payment_processors`

This command checks every second for open payment routes and creates a
separate thread to execute its `process` method.

Usage: `django-admin run_payment_processors```


## Commands for hub management and operation

These commands are supposed to be used by hub operators when setting
up new payment networks programatically or those that feel more
comfortable operating through the command line. The functionality
provided here should also be available on the admin site

### `register_blockchain`

Saves a new blockchain into the database. Optionally, you can provide
the url of the provider that will be used to connect to it.

Usage: `django-admin register_blockchain --id CHAIN_ID --name NAME [--provider-url PROVIDER]`

### `load_chain_data`

Loads all chain information according to chainlist schema. The default
url is https://chainid.network/chains.json

Usage: `django-admin load_chain_data [--url URL]`


### `load_erc20_token`

Gets information about any token that complies with the ERC20 standard
at any ethereum-compatible blockchain. Optionally adds the token as
listed by the hub.

Usage: `django-admin load_erc20_token --chain-id CHAIN_ID --address ADDRESS [--listed]`


### `load_token_list`

Loads [token lists](https://tokenlists.org/) and saves all the
information into the database.

Usage: `django-admin load_token_list <token_list_url> <token_list_url>`


## Functions for development

These commands are useful mostly when developing or debugging the application.

### `fix_user_setup`

Ensures all user and wallet-related models are created. Use it when/if
you run migrations that destroy the UserAccount, UserPreferences
entries.

Usage: `django-admin fix_user_setup`

### `mint_test_tokens`

Calls mint function on tokens (should only work for test tokens). This
requires the private key of the account, and it requires also that the
account is funded with the enough ETH (or equivalent native token) to
execute a transaction on the blockchain.

Usage: `django-admin mint_test_tokens -a ACCOUNT -t TOKEN [--amount AMOUNT] --chain-id CHAIN_ID`

### `record_transactions`

Queries the blockchain for the transactions given by
`transaction_hash` and saves them in the database.

Usage: `django-admin record_transactions --chain CHAIN_ID <transaction_hash> <transaction_hash>`
