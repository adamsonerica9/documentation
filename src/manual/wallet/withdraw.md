Whenever you want to pay someone using your account balance or you
just want to take some of the funds to a wallet that you control, you
will need to make an **external transfer**, also called a
**withdrawal**. External transfers will require fees (unless you hub
operator defines a policy that waives transfer fees[^1])

[^1]:
    Operator-defined policies are not implemented yet. The current
    policy is that the users pays for any transfers on web3 and that
    Raiden transfers are free and paid by the [Treasury](/topics/accounting).

To initiate a transfer, you can go to the **Funding** page, where you
see the [list of tokens you are tracking](../../token_management.md).
For those tokens that you have a positive balance, the "Transfer"
button will be enabled.

![Funding Dashboard](/assets/images/manual/funding_list.png)

---

Clicking the **Transfer** button. You will see "pop-up" form with
information about transfers.

![Transfer Modal](/assets/images/manual/transfer_modal.png)

Fill the form with the information the proper recipient address[^2],
and everything is correct the transfer is going to scheduled.

[^2]:
    At the moment, the only type of validation that the hub will
    provide is whether the address is valid or not. More types of
    validations can be made (e.g, check that address belongs to a
    proper account and not some contract that the user wants to
    interact with) to assist users and catching up mistakes early.
